from pybuckets import BucketsException, BucketServer, server_registry, Bucket
from pybuckets import BucketDataReader
import cloudfiles
import operator

class CloudfilesException(BucketsException):
    def __init__(self,msg,e):
        self.msg = msg
        self.e = e
        
class CloudfilesServer(BucketServer):
    def __init__(self,**server_config):
        super(CloudfilesServer,self).__init__()
        self.username = server_config['username']
        self.api_key = server_config['api_key']
        self.connection = cloudfiles.get_connection(self.username, self.api_key)
        
    def keys(self):
        return map(operator.attrgetter('name'),self.connection.get_all_containers())
#    
    def get(self,name):
        return CloudfilesBucket(self.connection.get_container(name),self) if name in self.keys() else None
#    
    def delete_bucket(self,key):
        self.connection.delete_container(key)
    
    def create_bucket(self,name,**kwargs):
        try :
            container = self.connection.create_container(name)
            return CloudfilesBucket(container,self)
        except Exception as e:
            raise CloudfilesException(e)
        
    def __str__(self):
        return 'CloudfilesServer(%s)' % self.username
            
class CloudfilesValueWrapper(object):
    def __init__(self,container, key, **kwargs):
        self.key = key
        self.container = container
        
    def __rshift__(self,target):
        obj = self.container.get_object(self.key)
        if isinstance(target,basestring) :
            with open(target, 'wb') as f :
                f.write(obj.read())
        elif hasattr(target,'write') :
            target.write(obj.read())
        else :
            raise Exception('invalid destination')
    
    def __lshift__(self,source):
        obj = self.container.create_object(self.key)
        if isinstance(source,basestring) :
            with open(source, 'r') as f :
                obj.write(f.read())
        elif hasattr(source,'read') :
            obj.write(source.read())
        else :
            raise Exception('invalid source')
    
class CloudfilesBucket(Bucket):
    def __init__(self,key,server):
        if isinstance(key,basestring) :
            self.key = key
            self.container = None
        elif isinstance(key,cloudfiles.container.Container) :
            self.key = key.name
            self.container = key
        self.server = server
    
    def __iter__(self):
        return self.keys().__iter__()
        
    def __str__(self):
        return 'CloudfilesBucket(%s,%s)' % (self.key, str(self.server))
    
    def __call__(self, key,**kwargs):
        return CloudfilesValueWrapper(self.container, key, **kwargs)
#    
    def keys(self):
        return map(operator.attrgetter('name'),self.container.get_objects())
##        
    def get_data(self,key):
        obj = self.container.get_object(key)
        return obj.read()
    
    def set_data(self,key,value):
        obj = self.container.create_object(key)
        obj.write(value)
#    
    def delete_key(self,key):
        self.container.delete_object(key)
        
server_registry['cloudfiles'] = CloudfilesServer

    

