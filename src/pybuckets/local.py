from pybuckets import BucketsException, BucketServer, server_registry, Bucket
import operator
import os
import shutil


class LocalFSException(BucketsException):
    def __init__(self,msg,e):
        self.msg = msg
        self.e = e
        
class LocalFSServer(BucketServer):
    def __init__(self,**server_config):
        super(LocalFSServer,self).__init__()
        self.path = server_config['path']
        self.server_config['_remove_non_empty_buckets'] = server_config.get('_remove_non_empty_buckets',False)
        if not os.path.isdir(self.path) :
            raise LocalFSException('%s must be a directory' % self.path)
        
    def get_bucket_names(self):
        return os.listdir(self.path)
    
    def get(self,name):
        return LocalFSBucket(name,self) if name in self.get_bucket_names() else None
    
    def delete_bucket(self,key):
        if self.server_config['_remove_non_empty_buckets'] :
            shutil.rmtree(os.path.join(self.path,key))
        else :
            os.rmdir(os.path.join(self.path,key))
    
    def create_bucket(self,name,**kwargs):
        try :
            path = os.path.join(self.path,name)
            if os.path.isdir(path) :
                return LocalFSBucket(name,self)
            else :
                os.mkdir(path)
                return LocalFSBucket(name,self)
        except Exception as e:
            raise LocalFSException(e)
        
    def __str__(self):
        return 'LocalFSServer(%s)' % self.path
            
class FileValueWrapper(object):
    def __init__(self, path,**kwargs):
        self.filepath = path
    
    def __rshift__(self, target):
        if isinstance(target, basestring) :
            with open(self.filepath, 'r') as fp :
                with open(target, 'wb') as f :
                    f.write(fp.read())
        elif hasattr(target, 'write') :
            with open(self.filepath, 'r') as fp :
                target.write(fp.read())
        else :
            raise Exception('invalid target')
            
    def __lshift__(self, source):
        if isinstance(source, basestring) :
            with open(self.filepath, 'wb') as fp :
                with open(source, 'r') as f :
                    fp.write(f.read())
        elif hasattr(source, 'read') :
            with open(self.filepath, 'wb') as fp :
                fp.write(source.read())
        else :
            raise Exception('invalid source')

class LocalFSBucket(Bucket):
    def __init__(self,key,server):
        self.key = key
        self.server = server
        self.fullpath = os.path.join(self.server.path,self.key)
    
    def __iter__(self):
        return self.keys().__iter__()
        
    def __str__(self):
        return 'LocalFSBucket(%s,%s)' % (self.key, str(self.server))
    
    def keys(self):
        return os.listdir(self.fullpath)
    
    def __call__(self, key,**kwargs):
        filepath = os.path.join(self.fullpath,key)
        return FileValueWrapper(filepath,**kwargs)
#        
    def get_data(self,key):
        filepath = os.path.join(self.fullpath,key)
        with open(filepath,'r') as f:
            value = f.read()
        return value
#    
    def set_data(self,key,value):
        filepath = os.path.join(self.fullpath,key)
        with open(filepath,'w') as f:
            f.write(value)
    
    def delete_key(self,key):
        filepath = os.path.join(self.fullpath,key)
        os.unlink(filepath)
        
server_registry['localfs'] = LocalFSServer

    

