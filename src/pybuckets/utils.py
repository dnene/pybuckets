def build_kw_from_params_and_object(params,obj,candidates) :
    kw = {}
    for key in candidates :
        if key in params : kw[key] = params[key]
        elif hasattr(obj,key) : kw[key] = getattr(obj,key)
    return kw