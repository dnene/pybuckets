from boto3 import resource
from pybuckets import Bucket, BucketServer, BucketsException, server_registry
from utils import build_kw_from_params_and_object
import operator

class Boto3S3BucketIterator(object):
    def __init__(self,sequence):
        self.iter = sequence.__iter__()
        
    def next(self):
        return self.iter.next()
    
class Boto3ValueWrapper(object):
    ALLOWED_EXTRA_ARGS = ('ServerSideEncryption', 'StorageClass')
    
    def __init__(self, boto3object, **kwargs):
        self.boto3object = boto3object
        for key, val in kwargs.items() :
            if key in self.ALLOWED_EXTRA_ARGS :
                self.__dict__[key] = val
        
    def __rshift__(self, target):
        if isinstance(target, basestring) :
            self.boto3object.download_file(target)
        elif hasattr(target, 'write') :
            target.write(self.boto3object.get()['Body'].read())
        else :
            raise Exception('invalid destination')
        
    def __lshift__(self, source):
        if isinstance(source, basestring) :
            self.boto3object.upload_file(source, ExtraArgs=build_kw_from_params_and_object({}, self, self.ALLOWED_EXTRA_ARGS))
        elif hasattr(source, 'read') :
            self.boto3object.put(Body=source, **build_kw_from_params_and_object({}, self, self.ALLOWED_EXTRA_ARGS))
        else :
            raise Exception('invalid source')
        
class Boto3S3Bucket(Bucket):
    def __init__(self, name, server):
        self.name = name
        self.server = server
        if self.server :
            self.s3bucket = self.server.s3_resource.Bucket(self.name)
        else :
            self.s3bucket = None
    
    def __iter__(self):
        return Boto3S3BucketIterator(self.keys())
    
    def __str__(self):
        return 'Boto3S3Bucket(%s,%s)' % (self.name, str(self.server))
    
    def keys(self):
        return map(operator.attrgetter('key'), self.s3bucket.objects.all())
        
    def __call__(self, key, **kwargs):
        obj = self.server.s3_resource.Object(self.s3bucket.name, key)
        return Boto3ValueWrapper(obj, **kwargs)

    def get_data(self, name):
        return self.server.s3_resource.Object(self.s3bucket.name, name).get()['Body'].read()
    
    def set_data(self, name, value):
        return self.server.s3_resource.Object(self.s3bucket.name, name).put(Body=value)
    
    def delete_key(self, key):
        self.server.s3_resource.Object(self.s3bucket.name, key).delete()

class Boto3S3Server(BucketServer):
    def __init__(self, **kwargs):
        super(Boto3S3Server, self).__init__()
        for key,val in kwargs.items() :
            if key in ('aws_access_key_id','aws_secret_access_key','region_name','api_version','use_ssl','verify',
                       'endpoint_url','aws_access_key_id','aws_secret_access_key','aws_session_token','config') :
                self.__dict__[key] = val
        
        self.s3_resource = resource('s3', 
                    **build_kw_from_params_and_object({}, self,
                        ('aws_access_key_id','aws_secret_access_key','region_name','api_version','use_ssl','verify',
                         'endpoint_url','aws_access_key_id','aws_secret_access_key','aws_session_token','config')))
        
    def keys(self):
        return map(operator.attrgetter('name'), self.s3_resource.buckets.all())
    
    def get(self, name):
        return Boto3S3Bucket(name, self) if name in self.keys() else None
    
    def delete_bucket(self, key):
        self.s3_resource.Bucket(key).delete()
    
    def create_bucket(self, name, **kwargs):
        try :
            bucket = self.s3_resource.create_bucket(Bucket=name,
                                **build_kw_from_params_and_object(kwargs, self,
                                    ('ACL','CreateBucketConfiguration','GrantFullControl','GrantRead','GrantReadACP','GrantWrite',
                                     'GrantWriteACP')))
            return self.create_using_boto3_bucket(bucket)
        except Exception as e:
            raise BucketsException(e)
        
    def __str__(self):
        return 'Boto3S3Server'
            
    def create_using_boto3_bucket(self, boto3_bucket):
        bucket = Boto3S3Bucket(boto3_bucket.name, self)
        return bucket

server_registry['boto3s3'] = Boto3S3Server

