from pybuckets import Bucket, MethodNotSupportedException, bucket_registry
import memcache

class MemcacheBucket(Bucket):
    def __init__(self,urls):
        self.memc = memcache.Client(urls)
        
    def __iter__(self):
        return self.keys().__iter__()
        
    def __str__(self):
        return 'MemcacheBucket(%s,%s)' % (self.key, str(self.server))
    
    def keys(self):
        return MethodNotSupportedException(self.keys)
#        
    def get_data(self,key):
        return self.memc.get(key)
#    
    def set_data(self,key,value):
        self.memc.set(key,value)
    
    def delete_key(self,key):
        self.memc.delete(key)
        
bucket_registry['localfs'] = MemcacheBucket

    

