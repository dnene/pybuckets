"""
Core pybuckets library
"""

server_registry = {}
"""Registry for autoregistration of various component libraries"""

bucket_registry = {}
""" Registry for autoregistration of various component libraries supporting just a bucket"""

class BucketsException(Exception):
    """Exception class to wrap various exceptions"""
    def __init__(self,e):
        self.e = e
    def __str__(self):
        return 'BucketsException(%s)' % str(self.e)
    def __repr__(self):
        return 'BucketsException(%s)' % str(self.e)
    
class DuplicateBucketNameException(BucketsException):
    "A exception indicating a duplicate bucket name"
    def __init__(self,e,name):
        super(DuplicateBucketNameException,self).__init__(e)
        self.name = name
    def __str__(self):
        return 'DuplicateBucketNameException(%s)' % str(self.name)
    def __repr__(self):
        return 'DuplicateBucketNameException(%s)' % str(self.name)

class MethodNotSupportedException(BucketsException):
    """A exception indicating that the particular method is not supported 
    by the underlying component library"""
    def __init__(self,method):
        super(MethodNotSupportedException,self).__init__(None)
        self.method = method
    def __str__(self):
        return 'MethodNotSupportedException(%s)' % str(self.method)
    def __repr__(self):
        return 'MethodNotSupportedException(%s)' % str(self.method)

class BucketDataReader(object):
    "Reader class to allow controlled access to reading data streams"
    def __init__(self,pointer,reader):
        self.pointer = pointer
        self.reader = reader
    def __enter__(self):
        return self
    def read(self):
        return self.reader.read()
    def __exit__(self,exc_type, exc_value, traceback):
        print exc_type, exc_value, traceback
#        self.pointer.close()
        
class Bucket(object):
    "A bucket"
    def __init__(self,name,server = None):
        self.name = name
        self.server = server
    def __getitem__(self,key):
        return self.get_data(key)
    def __setitem__(self,key,val):
        return self.set_data(key,val)
    def __delitem__(self,key):
        return self.delete_key(key)
    def __str__(self):
        return 'Bucket(%s,%s)' % (self.name, str(self.server))
    @classmethod
    def get_bucket(cls,name,**creds):
        return bucket_registry[name](**creds)
        
class BucketServerIterator(object):
    "A bucket server iterator"
    def __init__(self,server):
        self.server = server
        self.keys = self.server.keys().__iter__()
    def __iter__(self):
        return self
    def next(self):
        return self.server.get(self.keys.next())
        
class BucketServer(object):
    "A bucket server"
    def __init__(self):
        self.server_config = {}
    def __len__(self):
        return len(self.keys())
    def __iter__(self):
        return BucketServerIterator(self)
    def __getitem__(self,key):
        return self.get(key)
    def __delitem__(self,key):
        return self.delete_bucket(key)
    @classmethod
    def get_server(cls,name,**creds):
        return server_registry[name](**creds)


    
        
